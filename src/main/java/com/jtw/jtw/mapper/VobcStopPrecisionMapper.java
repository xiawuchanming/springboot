package com.jtw.jtw.mapper;

import org.apache.ibatis.annotations.Insert;

import com.jtw.jtw.domain.VehicleZhoucheng;
import com.jtw.jtw.domain.VobcStopPrecision;

public interface VobcStopPrecisionMapper {
    @Insert("INSERT INTO vobc_stop_precision VALUES (#{stamp}, #{trainno}, #{stopErrorNum})")
    void insert(VobcStopPrecision data);

}
