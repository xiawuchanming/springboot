package com.jtw.jtw.mapper;

import org.apache.ibatis.annotations.Insert;

import com.jtw.jtw.domain.AlarmStatistics;
import com.jtw.jtw.domain.JidianReport;

public interface JidianMapper {
    @Insert("INSERT INTO jidian_report VALUES (#{eventTime}, #{oneAlarmCount}, #{deviceType})")
    void insert(JidianReport data);

}
