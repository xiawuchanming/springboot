package com.jtw.jtw.mapper;

import org.apache.ibatis.annotations.Insert;

import com.jtw.jtw.domain.VehicleTamian;
import com.jtw.jtw.domain.VehicleWendu;

public interface VehicleWenduMapper {
    @Insert("INSERT INTO vehicle_wendu VALUES (#{eventTime}, #{wenduAlarm}, #{stateNum})")
    void insert(VehicleWendu data);

}
