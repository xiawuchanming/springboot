package com.jtw.jtw.mapper;

import org.apache.ibatis.annotations.Insert;

import com.jtw.jtw.domain.VobcStopPrecision;
import com.jtw.jtw.domain.XianluReport;

public interface XianluMapper {
    @Insert("INSERT INTO xianlu_report VALUES (#{eventDate}, #{percentPass}, #{percentFail})")
    void insert(XianluReport data);

}
