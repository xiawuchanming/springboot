package com.jtw.jtw.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.jtw.jtw.domain.AlarmStatistics;

public interface AlarmStatisticsMapper {
	
	@Select("SELECT * FROM alarm_statistics")
    @Results({
        @Result(property = "dataDate",  column = "data_date"),
        @Result(property = "sysNo", column = "sys_no"),
        @Result(property = "centerNo", column = "center_no"),
        @Result(property = "alarmLevel", column = "alarm_level"),
        @Result(property = "alarmNums", column = "alarm_nums")
    })
    List<AlarmStatistics> getAll();

    @Select("SELECT * FROM alarm_statistics WHERE id = #{id}")
    @Results({
    	@Result(property = "dataDate",  column = "data_date"),
        @Result(property = "sysNo", column = "sys_no"),
        @Result(property = "centerNo", column = "center_no"),
        @Result(property = "alarmLevel", column = "alarm_level"),
        @Result(property = "alarmNums", column = "alarm_nums")
    })
    AlarmStatistics getOne(String id);

    //@Insert("INSERT INTO alarm_statistics(data_date,sys_no,center_no,alarm_level,alarm_nums) VALUES <foreach collection=\"listAlarm\" item=\"i\" index=\"index\" separator=\",\">(#{i.dataDate}, #{i.sysNo}, #{i.centerNo}, #{i.alarmLevel}, #{i.alarmNums}) </foreach>")
    @Insert("INSERT INTO alarm_statistics(data_date,sys_no,center_no,alarm_level,alarm_nums) VALUES (#{dataDate}, #{sysNo}, #{centerNo}, #{alarmLevel}, #{alarmNums})")
    void insert(AlarmStatistics data);



}
