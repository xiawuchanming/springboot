package com.jtw.jtw.mapper;

import org.apache.ibatis.annotations.Insert;

import com.jtw.jtw.domain.JidianReport;
import com.jtw.jtw.domain.VehicleTamian;

public interface VehicleTamianMapper {
    @Insert("INSERT INTO vehicle_tamian VALUES (#{eventTime}, #{tamianAlarm}, #{stateNum})")
    void insert(VehicleTamian data);

}
