package com.jtw.jtw.mapper;

import org.apache.ibatis.annotations.Insert;

import com.jtw.jtw.domain.VehicleWendu;
import com.jtw.jtw.domain.VehicleZhoucheng;

public interface VehicleZhouchengMapper {
    @Insert("INSERT INTO vehicle_zhoucheng VALUES (#{eventTime}, #{zhouchengAlarm}, #{stateNum})")
    void insert(VehicleZhoucheng data);

}
