package com.jtw.jtw.service;

import javax.jms.Destination;

import org.springframework.stereotype.Service;

@Service
public interface MessageSenderService {
    void dealAlarmMessage(String msg);
    void dealAlarmMessage(String msg,Destination destination);
    void sendMessage(Destination destination, final String message);

}
