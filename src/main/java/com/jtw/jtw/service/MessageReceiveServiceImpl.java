package com.jtw.jtw.service;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageReceiveServiceImpl implements MessageReceiveService{
	 //@JmsListener(destination = "test")  
	    public void receiveQueue(String text) {  
	        System.out.println("Consumer收到的报文为:"+text);  
	    }  

}
