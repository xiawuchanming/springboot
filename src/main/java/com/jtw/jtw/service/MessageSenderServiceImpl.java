package com.jtw.jtw.service;

import javax.jms.Destination;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class MessageSenderServiceImpl implements MessageSenderService{
	@Autowired // 也可以注入JmsTemplate，JmsMessagingTemplate对JmsTemplate进行了封装  
    private JmsMessagingTemplate jmsTemplate;  
    // 发送消息，destination是发送到的队列，message是待发送的消息  
    public void sendMessage(Destination destination, final String message){  
        jmsTemplate.convertAndSend(destination, message);  
    }  
    public void dealAlarmMessage(String msg,Destination destination){
    	if (msg!=null) {
    		//AlarmMessage alarmMessage = JsonUtils.toObjectBean(msg, AlarmMessage.class);
        	//String subsys = alarmMessage.getMsgHead().getSystemLabel();
        	//Destination destination = new ActiveMQQueue("TESTQUEUE");
        	sendMessage(destination, msg);
        	System.out.println("发送成功:"+msg);
		}
    }
	@Override
	public void dealAlarmMessage(String msg) {
		// TODO Auto-generated method stub
		
	}

}
