package com.jtw.jtw;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.ClassUtils;

import com.jtw.jtw.common.MqConfig;
import com.jtw.jtw.domain.AlarmStatistics;
import com.jtw.jtw.domain.JidianReport;
import com.jtw.jtw.domain.VehicleTamian;
import com.jtw.jtw.domain.VehicleWendu;
import com.jtw.jtw.domain.VehicleZhoucheng;
import com.jtw.jtw.domain.VobcStopPrecision;
import com.jtw.jtw.domain.XianluReport;
import com.jtw.jtw.mapper.AlarmStatisticsMapper;
import com.jtw.jtw.mapper.JidianMapper;
import com.jtw.jtw.mapper.VehicleTamianMapper;
import com.jtw.jtw.mapper.VehicleWenduMapper;
import com.jtw.jtw.mapper.VehicleZhouchengMapper;
import com.jtw.jtw.mapper.VobcStopPrecisionMapper;
import com.jtw.jtw.mapper.XianluMapper;
import com.jtw.jtw.service.MessageSenderService;
import com.jtw.jtw.utils.POIHandle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
@MapperScan("com.jtw.jtw.mapper")
public class JtwApplication implements CommandLineRunner {

	@Autowired
	MessageSenderService messageSenderService;
	@Autowired
	AlarmStatisticsMapper alarmStatisticsMapper;
	@Autowired
	JidianMapper jidianMapper;
	@Autowired
	VehicleTamianMapper vehicleTamianMapper;
	@Autowired
	VehicleWenduMapper vehicleWenduMapper;
	@Autowired
	VehicleZhouchengMapper vehicleZhouchengMapper;
	@Autowired
	VobcStopPrecisionMapper vobcStopPrecisionMapper;
	@Autowired
	XianluMapper xianluMapper;
	@Autowired
	MqConfig mqConfig;
	@Value("${application.type}")
	private int appType;

	public static void main(String[] args) {
		SpringApplication.run(JtwApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// do something
		// File file =new File("C:\\Users\\lijian\\Desktop\\tt.xlsx");
		// String
		// path=java.net.URLDecoder.decode(ClassLoader.getSystemResource("tt.xlsx").getPath(),"utf-8");//编译、打包通过但jar包运行不通过
		// String
		// path2=java.net.URLDecoder.decode(ClassUtils.getDefaultClassLoader().getResource("tt.xlsx").getPath(),"utf-8");
		// String
		// path2=ClassUtils.getDefaultClassLoader().getResource("tt.xlsx").toURI().getPath();
		if (appType == 0) {

			// 发送报表统计数据
			InputStream stream = this.getClass().getResourceAsStream("/doc/tt.xlsx");// 拼接获取路径编译通过但打包不通过
			List<AlarmStatistics> listAlarm = POIHandle.getAlarmStatistics(stream);
			for (AlarmStatistics one : listAlarm) {
				alarmStatisticsMapper.insert(one);
			}
			messageSenderService.dealAlarmMessage(listAlarm.toString(),
					new ActiveMQQueue(mqConfig.getAlarm_statistics()));
			if (stream != null)
				stream.close();

			stream = this.getClass().getResourceAsStream("/doc/tt.xlsx");
			List<JidianReport> listJD = POIHandle.getJidianReport(stream);
			for (JidianReport one : listJD) {
				jidianMapper.insert(one);
			}
			messageSenderService.dealAlarmMessage(listJD.toString(), new ActiveMQQueue(mqConfig.getJidian()));
			if (stream != null)
				stream.close();

			stream = this.getClass().getResourceAsStream("/doc/tt.xlsx");
			List<VehicleTamian> listTamian = POIHandle.getVehicleTamian(stream);
			for (VehicleTamian one : listTamian) {
				vehicleTamianMapper.insert(one);
			}
			messageSenderService.dealAlarmMessage(listTamian.toString(),
					new ActiveMQQueue(mqConfig.getVehicle_tamian()));
			if (stream != null)
				stream.close();

			stream = this.getClass().getResourceAsStream("/doc/tt.xlsx");
			List<VehicleWendu> listWendu = POIHandle.getVehicleWendu(stream);
			for (VehicleWendu one : listWendu) {
				vehicleWenduMapper.insert(one);
			}
			messageSenderService.dealAlarmMessage(listWendu.toString(), new ActiveMQQueue(mqConfig.getVehicle_wendu()));
			if (stream != null)
				stream.close();

			stream = this.getClass().getResourceAsStream("/doc/tt.xlsx");
			List<VehicleZhoucheng> list = POIHandle.getVehicleZhoucheng(stream);
			for (VehicleZhoucheng one : list) {
				vehicleZhouchengMapper.insert(one);
			}
			messageSenderService.dealAlarmMessage(list.toString(), new ActiveMQQueue(mqConfig.getVehicle_zhoucheng()));
			if (stream != null)
				stream.close();

			stream = this.getClass().getResourceAsStream("/doc/tt.xlsx");
			List<VobcStopPrecision> listPc = POIHandle.getStopPrecision(stream);
			for (VobcStopPrecision one : listPc) {
				vobcStopPrecisionMapper.insert(one);
			}
			messageSenderService.dealAlarmMessage(listPc.toString(),
					new ActiveMQQueue(mqConfig.getVobc_stop_precision()));
			if (stream != null)
				stream.close();

			stream = this.getClass().getResourceAsStream("/doc/tt.xlsx");
			List<XianluReport> listXianlu = POIHandle.getXianluReport(stream);
			for (XianluReport one : listXianlu) {
				xianluMapper.insert(one);
			}
			messageSenderService.dealAlarmMessage(listXianlu.toString(), new ActiveMQQueue(mqConfig.getXianlu()));
			if (stream != null)
				stream.close();

			System.out.println("Send Statistic Data Succeed!");
		} else if (appType == 1) {

			// 发送离线报警数据（队列）
			InputStream streamalarm = this.getClass().getResourceAsStream("/doc/alarm_queue.txt");// 拼接获取路径编译通过但打包不通过
			// FileInputStream fis = (FileInputStream)streamalarm;
			// Construct BufferedReader from InputStreamReader
			BufferedReader br = new BufferedReader(new InputStreamReader(streamalarm));
			String line = null;
			while ((line = br.readLine()) != null) {
				// messageSenderService.dealAlarmMessage(line,new
				// ActiveMQTopic("TOPIC_MSS_ALARM_INFO"));
				messageSenderService.dealAlarmMessage(line, new ActiveMQQueue("MSS_ZC_ALARM_INFO"));
			}
			System.out.println("Send Queue Data Succeed!");
			br.close();
		} else if (appType == 2) {
			// 发送离线报警数据（Topic）
			InputStream streamalarm = this.getClass().getResourceAsStream("/doc/alarm_topic.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(streamalarm));
			String line = null;
			while ((line = br.readLine()) != null) {
				messageSenderService.dealAlarmMessage(line, new ActiveMQTopic("TOPIC_MSS_ALARM_INFO"));
			}
			System.out.println("Send Topic Data Succeed!");
			br.close();
		}

	}
}
