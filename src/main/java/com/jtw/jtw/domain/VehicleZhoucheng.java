package com.jtw.jtw.domain;

import java.util.Date;

public class VehicleZhoucheng {
    private Date eventTime;

    private double zhouchengAlarm;

    private double stateNum;
    
    public VehicleZhoucheng(Date time,double count,double statenum){
    	this.eventTime=time;
    	this.zhouchengAlarm=count;
    	this.stateNum=statenum;
    }
    
    public VehicleZhoucheng(){
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public double getZhouchengAlarm() {
        return zhouchengAlarm;
    }

    public void setZhouchengAlarm(double zhouchengAlarm) {
        this.zhouchengAlarm = zhouchengAlarm;
    }

    public double getStateNum() {
        return stateNum;
    }

    public void setStateNum(double stateNum) {
        this.stateNum = stateNum;
    }
}