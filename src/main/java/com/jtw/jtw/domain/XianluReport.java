package com.jtw.jtw.domain;

import java.util.Date;

public class XianluReport {
    private Date eventDate;

    private Double percentPass;

    private Double percentFail;

    public XianluReport(Date time,Double percentPass,Double percentFail){
    	this.eventDate=time;
    	this.percentPass=percentPass;
    	this.percentFail=percentFail;
    }
    
    public XianluReport(){
    }
    	
    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Double getPercentPass() {
        return percentPass;
    }

    public void setPercentPass(Double percentPass) {
        this.percentPass = percentPass;
    }

    public Double getPercentFail() {
        return percentFail;
    }

    public void setPercentFail(Double percentFail) {
        this.percentFail = percentFail;
    }
}