package com.jtw.jtw.domain;

import java.util.Date;

public class VobcStopPrecision {
    private Date stamp;

    private String trainno;

    private double stopErrorNum;
    
    public VobcStopPrecision(Date time,String trainno,double stopErrorNum){
    	this.stamp=time;
    	this.trainno=trainno;
    	this.stopErrorNum=stopErrorNum;
    }
    
    public VobcStopPrecision(){
    }

    public Date getStamp() {
        return stamp;
    }

    public void setStamp(Date stamp) {
        this.stamp = stamp;
    }

    public String getTrainno() {
        return trainno;
    }

    public void setTrainno(String trainno) {
        this.trainno = trainno == null ? null : trainno.trim();
    }

    public double getStopErrorNum() {
        return stopErrorNum;
    }

    public void setStopErrorNum(double stopErrorNum) {
        this.stopErrorNum = stopErrorNum;
    }
}