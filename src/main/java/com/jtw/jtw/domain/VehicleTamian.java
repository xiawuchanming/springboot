package com.jtw.jtw.domain;

import java.util.Date;

public class VehicleTamian {
    private Date eventTime;

    private double tamianAlarm;

    private double stateNum;
    
    public VehicleTamian(Date time,double count,double statenum){
    	this.eventTime=time;
    	this.tamianAlarm=count;
    	this.stateNum=statenum;
    }
    public VehicleTamian(){
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public double getTamianAlarm() {
        return tamianAlarm;
    }

    public void setTamianAlarm(double tamianAlarm) {
        this.tamianAlarm = tamianAlarm;
    }

    public double getStateNum() {
        return stateNum;
    }

    public void setStateNum(double stateNum) {
        this.stateNum = stateNum;
    }
}