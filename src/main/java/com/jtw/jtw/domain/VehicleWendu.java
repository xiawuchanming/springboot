package com.jtw.jtw.domain;

import java.util.Date;

public class VehicleWendu {
    private Date eventTime;

    private double wenduAlarm;

    private double stateNum;
    
    public VehicleWendu(Date time,double alarm,double statenum){
    	this.eventTime=time;
    	this.wenduAlarm=alarm;
    	this.stateNum=statenum;
    }
    
    public VehicleWendu(){
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public double getWenduAlarm() {
        return wenduAlarm;
    }

    public void setWenduAlarm(double wenduAlarm) {
        this.wenduAlarm = wenduAlarm;
    }

    public double getStateNum() {
        return stateNum;
    }

    public void setStateNum(double stateNum) {
        this.stateNum = stateNum;
    }
}