package com.jtw.jtw.domain;

import java.util.Date;

public class AlarmStatistics {
    private Date dataDate;

    private String sysNo;

    private String centerNo;

    private double alarmLevel;

    private double alarmNums;
    
    public AlarmStatistics(Date datadate,String sysno,String centerno,double level,double alarmnums){
    	this.dataDate=datadate;
    	this.sysNo=sysno;
    	this.centerNo=centerno;
    	this.alarmLevel=level;
    	this.alarmNums=alarmnums;
    }
    public AlarmStatistics(){
    }

    public Date getDataDate() {
        return dataDate;
    }

    public void setDataDate(Date dataDate) {
        this.dataDate = dataDate;
    }

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo == null ? null : sysNo.trim();
    }

    public String getCenterNo() {
        return centerNo;
    }

    public void setCenterNo(String centerNo) {
        this.centerNo = centerNo == null ? null : centerNo.trim();
    }

    public double getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(Integer alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    public double getAlarmNums() {
        return alarmNums;
    }

    public void setAlarmNums(Integer alarmNums) {
        this.alarmNums = alarmNums;
    }
}