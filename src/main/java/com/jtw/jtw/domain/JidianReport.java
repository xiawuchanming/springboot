package com.jtw.jtw.domain;

import java.util.Date;

public class JidianReport {
	private Date eventTime;

	private double oneAlarmCount;

	private String deviceType;

	public JidianReport(Date time, double count, String type) {
		this.eventTime = time;
		this.oneAlarmCount = count;
		this.deviceType = type;
	}

	public JidianReport() {
	}

	public Date getEventTime() {
		return eventTime;
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	public double getOneAlarmCount() {
		return oneAlarmCount;
	}

	public void setOneAlarmCount(Integer oneAlarmCount) {
		this.oneAlarmCount = oneAlarmCount;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType == null ? null : deviceType.trim();
	}
}