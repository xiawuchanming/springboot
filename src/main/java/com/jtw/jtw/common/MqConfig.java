package com.jtw.jtw.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="mq.queue")
@PropertySource("classpath:props/mq.properties")
public class MqConfig {
	private String jidian;
	private String xianlu;
	private String vehicle_wendu;
	private String vehicle_zhoucheng;
	private String vehicle_tamian;
	private String vobc_stop_precision;
	private String alarm_statistics;
	private String alarm_info;
	
	public String getAlarm_info() {
		return alarm_info;
	}
	public void setAlarm_info(String alarm_info) {
		this.alarm_info = alarm_info;
	}
	public String getJidian() {
		return jidian;
	}
	public void setJidian(String jidian) {
		this.jidian = jidian;
	}
	public String getXianlu() {
		return xianlu;
	}
	public void setXianlu(String xianlu) {
		this.xianlu = xianlu;
	}
	public String getVehicle_wendu() {
		return vehicle_wendu;
	}
	public void setVehicle_wendu(String vehicle_wendu) {
		this.vehicle_wendu = vehicle_wendu;
	}
	public String getVehicle_zhoucheng() {
		return vehicle_zhoucheng;
	}
	public void setVehicle_zhoucheng(String vehicle_zhoucheng) {
		this.vehicle_zhoucheng = vehicle_zhoucheng;
	}
	public String getVehicle_tamian() {
		return vehicle_tamian;
	}
	public void setVehicle_tamian(String vehicle_tamian) {
		this.vehicle_tamian = vehicle_tamian;
	}
	public String getVobc_stop_precision() {
		return vobc_stop_precision;
	}
	public void setVobc_stop_precision(String vobc_stop_precision) {
		this.vobc_stop_precision = vobc_stop_precision;
	}
	public String getAlarm_statistics() {
		return alarm_statistics;
	}
	public void setAlarm_statistics(String alarm_statistics) {
		this.alarm_statistics = alarm_statistics;
	}
	
	
}
