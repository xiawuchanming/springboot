package com.jtw.jtw.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonUtils {
	
	private static ObjectMapper mapper = null;

	static {
		mapper = new ObjectMapper();
		mapper.setLocale(Locale.getDefault());
		mapper.setTimeZone(TimeZone.getDefault());
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
	}

	public static ObjectMapper getMapper() {
		if (null == mapper) {
			mapper = new ObjectMapper();
			mapper.setLocale(Locale.getDefault());
			mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		}
		return mapper;
	}

	public static String toString(Object obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	}

 
	public static <T> T toObjectBean(String jsonStr, Class<T> objClass) {
		try {
			return mapper.readValue(jsonStr, objClass);
		} catch (Exception e) {
			e.printStackTrace();
			return  null;
		}
	}

//	public static List<SubsysEntity> getSubsysList(String jsonStr, Class<? extends Collection> collectionCls,
//			Class<?> eleCls) {
//		try {
//			return mapper.readValue(jsonStr, getType(collectionCls, eleCls));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return Collections.emptyList();
//	}

//	public static List<SwitchCurve> getSwitchCurveList(String jsonStr, Class<? extends Collection> collectionCls,
//			Class<?> eleCls){
//		try {
//			return mapper.readValue(jsonStr, getType(collectionCls, eleCls));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return Collections.emptyList();
//	}
	public static JavaType getType(Class<? extends Collection> collectionClass, Class<?> elementClass) {
		return mapper.getTypeFactory().constructCollectionType(collectionClass, elementClass);
	}

	public static ObjectNode newObjectNode() {
		return mapper.createObjectNode();
	}

	public static ArrayNode newArrayNode() {
		return mapper.createArrayNode();
	}

//	public static String createMsg(String createtime, String dest, String wid, String taskid, String content,
//			MessageType type) {
//		ObjectNode msg = mapper.createObjectNode();
//		msg.put("wid", wid).put("dest", dest).put("msg", content).put("type", type.getValue()).put("createtime",
//				createtime);
//
//		return msg.toString();
//	}
//	
//	public static String convertMsg( Message m) {
//		ObjectNode msg = mapper.createObjectNode();
//		msg.put("id",m.getId())
//		.put("dest", m.getDest())
//		.put("destType",m.getDestType())
//		.put("content", m.getContent())
//		.put("type", m.getType().getValue())
//		.put("createtime",
//				m.getCreateTime());
//
//		return msg.toString();
//	}

}
