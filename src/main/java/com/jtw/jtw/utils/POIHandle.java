package com.jtw.jtw.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.jtw.jtw.domain.AlarmStatistics;
import com.jtw.jtw.domain.JidianReport;
import com.jtw.jtw.domain.VehicleTamian;
import com.jtw.jtw.domain.VehicleWendu;
import com.jtw.jtw.domain.VehicleZhoucheng;
import com.jtw.jtw.domain.VobcStopPrecision;
import com.jtw.jtw.domain.XianluReport;

public class POIHandle {
	
	public static List<AlarmStatistics> getAlarmStatistics(InputStream file)
			throws IOException {
		List<AlarmStatistics> list = new ArrayList<AlarmStatistics>();
		Workbook book = null;
//		String fname=file.getName(); 
//		String xls = fname.substring(fname.lastIndexOf("."));
		 InputStream filestream = file;
		try {
//			if (xls.endsWith(".xls")) {
//				book = new HSSFWorkbook(filestream);
//			} else if (xls.endsWith(".xlsx")) {
//				book = new XSSFWorkbook(filestream);
//			} else {
//				throw new Exception("不是Excel文件");
//			}
			book = new XSSFWorkbook(filestream);
			Sheet s = book.getSheetAt(0);
			int first = s.getFirstRowNum();
			int last = s.getLastRowNum();
			if (first != last && last >= 1) {
				for (int row = 1; row < last; row++) {
					Row r = s.getRow(row);
					Date data=r.getCell(0).getDateCellValue();
					String sysNo=GetValueByType(r.getCell(1));
					String centerNo=GetValueByType(r.getCell(2));
					String level=GetValueByType(r.getCell(3));
					String alarmNums=GetValueByType(r.getCell(4));
					AlarmStatistics wd = new AlarmStatistics(data,sysNo,centerNo,Double.parseDouble(level),Double.parseDouble(alarmNums));
					list.add(wd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != book)
					book.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public static List<JidianReport> getJidianReport(InputStream file)
			throws IOException {
		List<JidianReport> list = new ArrayList<JidianReport>();
		Workbook book = null;
//		String fname=file.getName(); 
//		String xls = fname.substring(fname.lastIndexOf("."));
		 InputStream filestream =file;
		try {
//			if (xls.endsWith(".xls")) {
//				book = new HSSFWorkbook(filestream);
//			} else if (xls.endsWith(".xlsx")) {
//				book = new XSSFWorkbook(filestream);
//			} else {
//				throw new Exception("不是Excel文件");
//			}
			book = new XSSFWorkbook(filestream);
			Sheet s = book.getSheetAt(1);
			int first = s.getFirstRowNum();
			int last = s.getLastRowNum();
			if (first != last && last >= 1) {
				for (int row = 1; row < last; row++) {
					Row r = s.getRow(row);
					Date time=r.getCell(0).getDateCellValue();
					String count=GetValueByType(r.getCell(1));
					String type=GetValueByType(r.getCell(2));
					if(count==""||type=="")
						continue;
					JidianReport wd = new JidianReport(time,Double.parseDouble(count),type);
					list.add(wd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != book)
					book.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	public static List<VehicleTamian> getVehicleTamian(InputStream file)
			throws IOException {
		List<VehicleTamian> list = new ArrayList<VehicleTamian>();
		Workbook book = null;
//		String fname=file.getName(); 
//		String xls = fname.substring(fname.lastIndexOf("."));
		 InputStream filestream = file;
		//InputStream filestream = file.getInputStream();
		// MultipartFile file2=new MultipartFile("");
		try {
//			if (xls.endsWith(".xls")) {
//				book = new HSSFWorkbook(filestream);
//			} else if (xls.endsWith(".xlsx")) {
//				book = new XSSFWorkbook(filestream);
//			} else {
//				throw new Exception("不是Excel文件");
//			}
			book = new XSSFWorkbook(filestream);
			Sheet s = book.getSheetAt(2);
			int first = s.getFirstRowNum();
			int last = s.getLastRowNum();
			if (first != last && last >= 1) {
				for (int row = 1; row < last; row++) {
					Row r = s.getRow(row);
					Date time=r.getCell(0).getDateCellValue();
					String tamianAlarm=GetValueByType(r.getCell(1));
					String stateNum=GetValueByType(r.getCell(2));
					VehicleTamian wd = new VehicleTamian(time,Double.parseDouble(tamianAlarm),Double.parseDouble(stateNum));
					list.add(wd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != book)
					book.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public static List<VehicleWendu> getVehicleWendu(InputStream file)
			throws IOException {
		List<VehicleWendu> list = new ArrayList<VehicleWendu>();
		Workbook book = null;
//		String fname=file.getName(); 
//		String xls = fname.substring(fname.lastIndexOf("."));
		 InputStream filestream = file;
		//InputStream filestream = file.getInputStream();
		// MultipartFile file2=new MultipartFile("");
		try {
//			if (xls.endsWith(".xls")) {
//				book = new HSSFWorkbook(filestream);
//			} else if (xls.endsWith(".xlsx")) {
//				book = new XSSFWorkbook(filestream);
//			} else {
//				throw new Exception("不是Excel文件");
//			}
			book = new XSSFWorkbook(filestream);
			Sheet s = book.getSheetAt(3);
			int first = s.getFirstRowNum();
			int last = s.getLastRowNum();
			if (first != last && last >= 1) {
				for (int row = 1; row < last; row++) {
					Row r = s.getRow(row);
					Date time=r.getCell(0).getDateCellValue();
					String wenduAlarm=GetValueByType(r.getCell(1));
					String stateNum=GetValueByType(r.getCell(2));
					VehicleWendu wd = new VehicleWendu(time,Double.parseDouble(wenduAlarm),Double.parseDouble(stateNum));
					list.add(wd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != book)
					book.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public static List<VehicleZhoucheng> getVehicleZhoucheng(InputStream file)
			throws IOException {
		List<VehicleZhoucheng> list = new ArrayList<VehicleZhoucheng>();
		Workbook book = null;
//		String fname=file.getName(); 
//		String xls = fname.substring(fname.lastIndexOf("."));
		 InputStream filestream = file;
		//InputStream filestream = file.getInputStream();
		// MultipartFile file2=new MultipartFile("");
		try {
//			if (xls.endsWith(".xls")) {
//				book = new HSSFWorkbook(filestream);
//			} else if (xls.endsWith(".xlsx")) {
//				book = new XSSFWorkbook(filestream);
//			} else {
//				throw new Exception("不是Excel文件");
//			}
			book = new XSSFWorkbook(filestream);
			Sheet s = book.getSheetAt(4);
			int first = s.getFirstRowNum();
			int last = s.getLastRowNum();
			if (first != last && last >= 1) {
				for (int row = 1; row < last; row++) {
					Row r = s.getRow(row);
					Date time=r.getCell(0).getDateCellValue();
					String wenduAlarm=GetValueByType(r.getCell(1));
					String stateNum=GetValueByType(r.getCell(2));
					VehicleZhoucheng wd = new VehicleZhoucheng(time,Double.parseDouble(wenduAlarm),Double.parseDouble(stateNum));
					list.add(wd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != book)
					book.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public static List<VobcStopPrecision> getStopPrecision(InputStream file)
			throws IOException {
		List<VobcStopPrecision> list = new ArrayList<VobcStopPrecision>();
		Workbook book = null;
//		String fname=file.getName(); 
//		String xls = fname.substring(fname.lastIndexOf("."));
		 InputStream filestream =file;
		//InputStream filestream = file.getInputStream();
		// MultipartFile file2=new MultipartFile("");
		try {
//			if (xls.endsWith(".xls")) {
//				book = new HSSFWorkbook(filestream);
//			} else if (xls.endsWith(".xlsx")) {
//				book = new XSSFWorkbook(filestream);
//			} else {
//				throw new Exception("不是Excel文件");
//			}
			book = new XSSFWorkbook(filestream);
			Sheet s = book.getSheetAt(5);
			int first = s.getFirstRowNum();
			int last = s.getLastRowNum();
			if (first != last && last >= 1) {
				for (int row = 1; row < last; row++) {
					Row r = s.getRow(row);
					Date time=r.getCell(0).getDateCellValue();
					String trainNo=GetValueByType(r.getCell(1));
					String errorNum=GetValueByType(r.getCell(2));
					VobcStopPrecision wd = new VobcStopPrecision(time,trainNo,Double.parseDouble(errorNum));
					list.add(wd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != book)
					book.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public static List<XianluReport> getXianluReport(InputStream file)
			throws IOException {
		List<XianluReport> list = new ArrayList<XianluReport>();
		Workbook book = null;
//		String fname=file.getName(); 
//		String xls = fname.substring(fname.lastIndexOf("."));
		 InputStream filestream = file;
		//InputStream filestream = file.getInputStream();
		// MultipartFile file2=new MultipartFile("");
		try {
//			if (xls.endsWith(".xls")) {
//				book = new HSSFWorkbook(filestream);
//			} else if (xls.endsWith(".xlsx")) {
//				book = new XSSFWorkbook(filestream);
//			} else {
//				throw new Exception("不是Excel文件");
//			}
			book = new XSSFWorkbook(filestream);
			Sheet s = book.getSheetAt(6);
			int first = s.getFirstRowNum();
			int last = s.getLastRowNum();
			if (first != last && last >= 1) {
				for (int row = 1; row < last; row++) {
					Row r = s.getRow(row);
					Date time=r.getCell(0).getDateCellValue();
					double percentPass=r.getCell(1).getNumericCellValue();
					double percentFail=r.getCell(2).getNumericCellValue();
					XianluReport wd = new XianluReport(time,percentPass,percentFail);
					list.add(wd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != book)
					book.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	
	private static String GetValueByType(Cell cell) {
		if(cell==null)
			return "";
		int type = cell.getCellType();
		String cellvalue = "";
		switch (type) {
		case 1:
			cellvalue = cell.getStringCellValue();
			break;
		case 0:
			cellvalue = cell.getNumericCellValue() + "";
		default:
			break;
		}
		return cellvalue;
	}
	
	
	
}

